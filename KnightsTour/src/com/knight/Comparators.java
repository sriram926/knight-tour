package com.knight;

import java.util.Comparator;
import com.knight.Point;
/*
 * This is a comparator class. It is used to sort the knight moves as per the priority.
 * This class will be a helper class for the Collection class to sort the Points object.
 * The order of the sort will be ascending order.
 * */
public class Comparators
{
	public static Comparator<Point> comp = new Comparator<Point>() {
        @Override
        public int compare(Point p1, Point p2) 
        {
        	if(p1.priority < p2.priority)
        	{
        		return -1;
        	}
        	return 1;
        }
    };
}
