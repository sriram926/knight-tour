package com.knight;

/*
 * Objects of this class are used to store the moves of the knight.
 * This class consists of x,y, priority. x represents the X coordinate 
 * of the square on the chess board.
 * y represents the Y coordinate of the square on the chess board.
 * priority is the used to order the moves based on the heuristic.
 * */
public class Point 
{
	int x;
	int y;
	int priority = -1;
	
	/*
	 * This is the constructor of the class. It takes, x,y which are 
	 * X,Y coordinates of the square.
	 * */
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	/*
	 * This method sets the priority of the move.
	 * */
	public void setpriority(int p)
	{
		this.priority = p;
	}
	
	/*
	 * This method returns the priority of the move.
	 * */
	public int getPriority()
	{
		return this.priority;
	}

}
