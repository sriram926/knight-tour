/*
 * Improved performance of Backtracking algorithm with Heuristics.
 * 
 * Problem Statement:  
 * 	Knight�s tour is a classical problem in computer science. In this problem, 
 * the knight piece has to visit every other square in the chess board without 
 * revisiting the already visited square. Let us assume that knight starts from 
 * an arbitrary square on the board, and now it has to visit all the remaining 63 
 * squares on the 8 x 8 board. Knight has a typical movement on the chess board. 
 * Record the number of backtracking calls made and time consumed. 
 * 
 * Solution:
 * 		Knight's tour is solved using backtracking algorithm. In this method, the 
 * look for all the possible paths in a tree. Backtracking algorithm is similar 
 * to depth-first search in a tree. For a 8 x 8 chess board, if we are able to find 
 * the tree with depth of 64, then it will be the path of knight.
 * 
 * Algorithm:
 * 	1. Initialize the board: mark all squares unvisited (-1 in this case)
 *  2. Mark start vertex as visited by putting move number into it.
 *  3. Get all the possible moves (Not visited & inside the boundaries)
 *  4. Order the moves based on heuristics.
 *  5. for all available moves
 *  	 5.1 vertex as visited by putting move number into it.
 *       5.2 Start tour from that node.
 *       5.3 If all nodes visited:
 *       		 print tour
 *       	 else: 
 *       		Revert current step by marking current square -1
				increase backtracking count
 *   Repeat the program for different heuristics.
 *   
 * Implementation: 
 * In this program, composite datatypes like classes are used. 
 * Square board is represented using two-dimensional integer array. Moves are 
 * stored as Point objects.
 * Point consists of x-coordinate, y-coordinate and priority of the step.
 * List of valid next moves will be stored in ArrayList implementation of List interface.
 * 
 * Libraries used: ArrayList to stored moves, Collections to sort the arrays.
 * 
 * Accessing functions; Stardard java call will be made to invoke functions. 
 * Heuristics is available as a new class method.
 * 
 * It moves two steps in one direction and one step in perpendicular direction, 
 * order can be altered. 
 * Its movement often represented using alphabet �L�. For the knight in position (i, j), 
 * the possible next moves are:
(i-2, j-1), (i-2, j+1), (i+2, j-1), (i+2, j+1), 
(i-1, j-2), (i-1, j+2), (i+1, j-2), (i+1, j+2) 
provided the next square is not visited and the next square is inside the chess board.
 
 * */

package com.knight;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class TheKnightsTour {
    private static int BOARD_LENGTH = 5;
    private static int BOARD_BREADTH = 5;
    private static int board[][];      
    private static  Point start = new Point(0,1);
    private static long backTrackCount = 0;
    private enum Heuristic {NO_HEURISTIC, WARNSDROFF, CORNER_EDGE};
    
    private static Heristics h = new Heristics(BOARD_LENGTH, BOARD_BREADTH);
    
    /*Creating points with move ordering 87341625*/
    private static final Point[] MOVES = new Point[]
    	{
    		getPoint(8), getPoint(7), getPoint(3), getPoint(4), 
	    	getPoint(1), getPoint(6), getPoint(2), getPoint(5)
        };
    
    /*
     * This is the main class which triggers the program. 
     * This method reads the board size and starting point of the knight 
     * and start the tour. This method starts the tour three times, each time 
     * with one specific heuristic. It outputs the tour from starting square 
     * to all other squares in the board. It also prints the no of backtracking calls
     * and time taken.    
     * */
    public static void main(String[] args) 
    {
    	Scanner sc = new Scanner(System.in);
    	System.out.println("Enter Board length & Breadth (space delimitter):");
    	BOARD_LENGTH = sc.nextInt();
    	BOARD_BREADTH= sc.nextInt();
    	
    	System.out.println("Enter start x,y (space delimitter):");
    	int x = sc.nextInt();
    	int y = sc.nextInt();
    	
    	start = new Point(x,y);
    	sc.close();
    	   	
    	
    	System.out.println("--------- Without Heuristics ---------");
    	startAnalysis(Heuristic.NO_HEURISTIC);
    	
    	System.out.println("---------With Heuristic 1 ---------");
    	startAnalysis(Heuristic.CORNER_EDGE);
    	
    	System.out.println("--------- With Heuristic 2 ---------");
    	startAnalysis(Heuristic.WARNSDROFF);
    }
    
    /*
     * This method start the actual tour in the program. 
     * This method initializes the board and records the timetaken to 
     * find the tour. It also records the backtracking calls and prints them on 
     * standard output. This method take the heuristic as input parameer. 
     * Based on the heuristic rest of the tour is proceeded.    
     * */
    private static void startAnalysis(Heuristic heuristic)
    {
    	intializeBoard(start);
    	long startTime = System.currentTimeMillis();
    	boolean isResultAvailable = startTour(start.x, start.y, heuristic);
    	long endTime = System.currentTimeMillis();
    	if(isResultAvailable)
    	{
    		printTour();
    	}
    	else
    	{
    		System.out.println("No Tour found.");
    	}
    	System.out.println("No of backtrack calls :" + backTrackCount);
    	System.out.println("Time Elapsed (ms):" + (endTime-startTime));
    	backTrackCount = 0;
    	System.out.println("\n");
    }
    
    /*
     * This method intializes the board dimensions. It marks all the square in 
     * the board as unvisited by setting them to -1. It also marks the starting 
     * square visited and sets its to move number.
     * */
    private static void intializeBoard(Point start_point)
    {
    	board = new int[BOARD_LENGTH][BOARD_BREADTH];
        for (int i = 0; i < BOARD_LENGTH; i++) {
            for (int j = 0; j < BOARD_BREADTH; j++) {
                board[i][j] = -1;
            }
        }
        board[start_point.x][start_point.y] = 1;
    }
    
    /*
     * This method prints the tour.
     * */
    private static void printTour() {
        for (int i = 0; i < BOARD_LENGTH; i++) {
            for (int j = 0; j < BOARD_BREADTH; j++) {
                System.out.printf("%-8d", board[i][j]);
            }
            System.out.printf("%n");
        }
    }

    /*
     * This method starts the tour. This method also specifies if the tour 
     * is possible or not. 
     * */
    public static boolean startTour(int sRow, int sCol, Heuristic heuristic) {
        if (solveTour(sRow, sCol, 2, heuristic)) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * This method recursively finds the tour. It keeps the track of current 
     * move number and current position of knight and type of heuristic. Based order 
     * them and chooses the best one. It tries to find the tour from that particular 
     * square and put the move number into the square and marks it visited. If 
     * it is unable to find the move, it goes back to previous square, marks this 
     * as unvisited and chooses the next best move.
     * */
    private static boolean solveTour(int row, int col, int move_num, Heuristic heuristic) {
        if (move_num > BOARD_LENGTH * BOARD_BREADTH) {
            return true;
        }

        List<Point> validMoves = getValidMoves(row, col);
        if(validMoves == null)
        	return false;
        
        switch(heuristic)
        {
        	case NO_HEURISTIC : break;
        	
        	case CORNER_EDGE : h.heuristic1(validMoves);
        						break;
        						
        	case WARNSDROFF : warnsdroff(validMoves);
        						break;
        }
        for (Point p : validMoves) {
                board[p.x][p.y] = move_num;
                move_num++;
                if (solveTour(p.x, p.y, move_num, heuristic)) {
                    return true;
                } else {
                    board[p.x][p.y] = -1;
                    move_num--;
                    backTrackCount ++;
                }
            }
        return false;
    }
    
    /*
     * This method orders the moves as per Warnsdroff algorithm.
     * Warnsdroff algorithm states that the choice of square has to be 
     * made that has minimal outward paths. This method receives the valid 
     * moves list, set the priority and sorts them using Java Collection library. 
     * */
    private static void warnsdroff(List<Point> validMovesList)
    {
        for(Point p : validMovesList)
        {
        	List<Point> temp = getValidMoves(p.x, p.y);
        	p.setpriority(temp.size()-1);
        }
        Collections.sort(validMovesList, Comparators.comp);
    }

    /*
     * This method calculates the number takes the knight position and 
     * return the list of possible moves that can be made from  that particular square. 
     * The condition that are considered while finding the 
     * next moves are that the next move should be
     * 		1. Unvisited.
     * 		2. Should be inside the chess board dimensions.
     * */
    private static List<Point> getValidMoves(int row, int col)
    {
    	List<Point> validMoves = new ArrayList<Point>();
    	for(Point p : MOVES)
    	{
    		 int nextRow = row + (int) p.x;
             int nextCol = col + (int) p.y;
             if(isValidMove(nextRow, nextCol))
             {
            	 validMoves.add(new Point(nextRow, nextCol));
             }
    	}
    	return validMoves;
    }
    
    /*
     * This method is a helper method for getValidMoves. This method takes the
     * square position and decides if it meets requirements are not.
     * */
    private static boolean isValidMove(int row, int col) {
        return ((row >= 0 && row < BOARD_LENGTH)
                && (col >= 0 && col < BOARD_LENGTH)
                && (board[row][col] == -1));
    }
 
    /*
     * This method is return the point based on the given input. This method is used to
     * while deciding the move ordering.
     * The input parameter n denote the position of square on the board. the ordering 
     * is designed in such a way that the position (i-1, j-2) from position (i, j)
     * is considered as 1 and the next moves are considered in closewise direction.
     * (i-2, j-1) is to be 8.
     * */
    private static Point getPoint(int n)
    {
    	switch(n)
    	{
	    	case 1: return new Point(-1, -2);
	    	case 2: return new Point(1, -2);
	    	case 3: return new Point(2, -1);
	    	case 4: return new Point(2, 1);
	    	case 5: return new Point(1, 2);
	    	case 6: return new Point(-1, 2);
	    	case 7: return new Point(-2, 1);
	    	case 8: return new Point(-2, -1);
	    	default: return null;
    	}
    }
}