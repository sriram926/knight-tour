package com.knight;

import java.util.Collections;
import java.util.List;

/*
 * This class sorts the valid moves based on heuristic one.
 * Heuristic one States that
 * 
	a.	Corner square has to be given top priority: 
		A corner square only two possible positions to be visited. 
		A knight needs two unvisited squares in middle of game to 
		have successful tour. 
		So by chance, if the knight do not choose corner square, 
		it is for sure the tour will not be completed 
		as the left over corner square can never be visited again. 
		To avoid these known mistakes, 
		corner square is given top priority.
	
	b.	Edge square has to be given next priority.
		Edge square at most will have four different square to be 
		visited from. 
		Even in a case of edge, corner square available, corner has 
		to be chosen, as edge will have one more 
		chance of visiting it. So after the corner square, edge square 
		has to be given priority.
	
	c.	Remaining square are to be chosen randomly.
	
	d. Any tie breaking is randomly addressed.

 * */
public class Heristics 
{
	int length, breadth;
	
	/*
	 * This is the constructor for the heuristic class. it initializes 
	 * the board dimensions which makes the 
	 * program aware of corner and edge square. based on the position of 
	 * the square the priority is given to the 
	 * the program to decide which is corner square 
	 * */
	public Heristics(int length, int breadth) {
		this.length = length;
		this.breadth = breadth;
	}

	/*
	 * This is the core module of this class. This method takes the List 
	 * of Point object and sorts it based on the heuristic 1.
	 * This method iterators all the points in the given list and 
	 * prioritizes it. Based on the priority set,
	 * Collection package uses the comparator to sort the points. 
	 * Based on the sorted order, tour will be modified as
	 * the knight chooses the top priority one. 
	 * */
	public void heuristic1(List<Point> validMovesList)
    {
        for(Point p : validMovesList)
        {
        	if(isCornerBox(p))
        	{
        		p.setpriority(1);
        	}
        	else if(isEdgeBox(p))
        	{
        		p.setpriority(2);
        	}
        	else
        	{
        		p.setpriority(3);
        	}
        }
        Collections.sort(validMovesList, Comparators.comp);
    }
    
	/*
	 * This method takes a point object and returns that that particular 
	 * square is either a corner one or not.
	 * If the square is corner one, it returns true, else false.
	 * This method uses the board dimensions to decide if the point is corner or not.  
	 * */
    private boolean isCornerBox(Point p)
    {
    	boolean isCorner = false;
    	if((p.x == 0 && p.y==0) ||
    			(p.x == 0 && p.y == length-1) ||
    			(p.x == breadth-1 && p.y ==0) ||
    			(p.x == breadth-1 && p.y == length-1))
    	{
    		isCorner = true;
    	}
    	return isCorner;
    }
    
    /*
	 * This method takes a point object and returns that that particular 
	 * square is either a edge one or not.
	 * If the square is edge square, it returns true, else false.
	 * This method uses the board dimensions to decide if the point is edge or 
	 * not. This method considers the 
	 * corner square also an edge as corner is one extreme end of the edge.  
	 * */
    private boolean isEdgeBox(Point p)
    {
    	boolean edge = false;
    	if((p.x == 0) || (p.y == 0) || (p.x == breadth-1) ||(p.y == length-1))
    	{
    		edge = true;
    	}
    	return edge;
    }
}
